module Images where

import qualified NaiveMatrix as NM

type Plane a = NM.Matrix a
data Image a = Image Int (Plane a) (Plane a) (Plane a)
type Images a = [Image a]

getImageType :: Image a -> Int
getImageType (Image t _ _ _) = t
