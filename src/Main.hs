module Main where

import System.Environment

import ReadCifar
import qualified NearestNeighbour as NN

main :: IO ()
main = do
    argv <- getArgs
    let path = head argv
    images <- readCifarTraining path
    tests <- readCifarTesting path

    let (correct, total) = NN.evaluate NN.distL1 images tests
        percentage = (fromIntegral correct) / (fromIntegral total :: Float) * 100
    putStrLn $ "Nearest neighbour with L1 distance: " ++ (show percentage) ++ "%"
