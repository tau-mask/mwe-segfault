module NaiveMatrix where

import qualified Data.Vector.Unboxed as V

data Matrix a = Matrix Int Int (V.Vector a)
                       deriving (Eq, Show)

getVals :: Matrix a -> V.Vector a
getVals (Matrix _ _ v) = v

matrixV :: (V.Unbox a) => Int -> V.Vector a -> Matrix a
matrixV cols vals
    | remainder /= 0 = error "Incorrect number of columns"
    | otherwise = Matrix cols rows vals
    where (rows, remainder) = divMod (V.length vals) cols
