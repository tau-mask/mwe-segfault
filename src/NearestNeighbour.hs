module NearestNeighbour where

import Data.List
import qualified Data.Vector.Unboxed as V

import Images
import qualified NaiveMatrix as NM

type DistFn a = Image a -> Image a -> Double

distLnPlane :: (Num a, V.Unbox a) => Plane a -> Plane a -> Int -> a
distLnPlane p1 p2 order = V.foldl1' (+) $ V.map ((^ order) . absl) diff
    where diff = V.zipWith (-) (NM.getVals p1) (NM.getVals p2)
          absl
            | even order = id
            | otherwise = abs

distL1 :: (Integral a, V.Unbox a) => DistFn a
distL1 (Image _ r1 g1 b1) (Image _ r2 g2 b2) =
    fromIntegral x
    where x = distLnPlane r1 r2 1 +
              distLnPlane g1 g2 1 +
              distLnPlane b1 b2 1

findNearest :: (Num a, Ord a) => DistFn a -> Images a -> Image a -> Int
findNearest distF images testImage = nearest $ foldl1' minDist neighbours
    where nearest (_, t) = t
          minDist n1@(d1, _) n2@(d2, _) = if d1 < d2 then n1 else n2
          neighbours = map (\i -> (distF testImage i, getImageType i)) images

evaluate :: (Num a, Ord a) => DistFn a -> Images a -> Images a -> (Int, Int)
evaluate distF images testImages = (countCorrect, length results)
    where countCorrect = length $ filter isCorrect results
          isCorrect (predicted, actual) = predicted == actual
          results = map (\i -> (findNearest distF images i, getImageType i)) testImages
