module ReadCifar where

import Data.Int
import qualified Data.Attoparsec.ByteString as AT
import qualified Data.ByteString as BS
import qualified Data.Vector.Unboxed as V
import Conduit
import Data.Conduit.Attoparsec

import Images
import qualified NaiveMatrix as NM

parseCifarPlane :: AT.Parser (Plane Int32)
parseCifarPlane = do
    packedPixels <- AT.take 1024
    let pixels = fromIntegral <$> BS.unpack packedPixels :: [Int32]
    return $ NM.matrixV 32 $ V.fromList pixels

parseCifarImage :: AT.Parser (Image Int32)
parseCifarImage = do
    imageType <- (fromIntegral <$> AT.anyWord8)
    planes <- sequence $ replicate 3 parseCifarPlane
    return (Image imageType (planes !! 0) (planes !! 1) (planes !! 2))

parseCifarFile :: AT.Parser (Images Int32)
parseCifarFile = do
    images <- AT.many' parseCifarImage
    AT.endOfInput
    return images

conduitReadFiles :: [String] -> IO (Images Int32)
conduitReadFiles x = runConduitRes
     $ yieldMany x
    .| awaitForever sourceFile
    .| sinkParser parseCifarFile

readCifarTraining :: String -> IO (Images Int32)
readCifarTraining path =
    conduitReadFiles [path ++ "/data_batch_1.bin"]

readCifarTesting :: String -> IO (Images Int32)
readCifarTesting path =
    conduitReadFiles [path ++ "/test_batch.bin"]
